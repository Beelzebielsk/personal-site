# Plans

- Choose between light and dark theme.
- A theme: this might be hard to pull off, but I'd work toward it. It
	will be broken up a lot like a resume, and have resume-like headings.
	It should start off super crisp and professional on the left, and then
	proceed to have "life" creep in on the right. I'd like for there to be
	a gradient that goes from nothing to sunrise as it proceeds right, and
	for horizontal rules to start accumulating small plants and end with
	some overgrowth on the right. It might start with some moss, then
	hanging moss, then bushes. Maybe eventually there's people. Maybe some
	sort of life is hanging off of the horizontal rules at the right.
	Perhaps the transition to "life" kicks off earlier and earlier with
	each next section. Maybe I throw away the resume shit entirely later
	on. This is a terrible idea. Let's see what happens. If I have to draw
	anything, I'll go for something like Doug.

# TODO

## Introduce yourself

I spend most of my time breathing. I breathe for an exhausting 168 hours
a week. Somehow, in the moments between managing my lungs, I live a
life.  It includes family and friends, somehow. Breathing takes a lot of
time out of my life; I often wonder about the things I'd do if I didn't
have to breathe. But then I remember that breathing is fulfilling. A
life spent not breathing is a life wasted.

## Projects I've Never Finished

- A game!
- A project!
- A *bigger* project! Unreal.
- A smaller project. Even this... unfinished.

## Education/Professional Experience, Relevant Skills

- Intermediate breathing
- College
- Typing

## List any Technology or blog links you reccomend

I recommend a lot of links! Anything that means getting a simpler
understanding of a concept. There's no such thing as too simple. "Smart
people" get things done not through mental magic, but through making the
world dumbed-down enough to be understandable. These posts make things
dumb in a good way.

## Include at least one image

## Post a blog entry about a technology you have used, or school topic
